# -*- Makefile -*-

PORT = 3000

help:
	@echo make lang: build translation files
	@echo make start-dev-server: start a development server

lang:
	cd l10n; make

start-dev-server:
	cd server && make start-dev-server
