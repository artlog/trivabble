/*global libD*/

window.libD = {
    l10n: function () {
        "use strict";

        const t = [];

        function f (lang, orig, translated) {
            if (!orig) {
                return (
                    (libD.lang && t[libD.lang] && t[libD.lang][lang])
                        ? t[libD.lang][lang]
                        : lang // lang is the default string
                );
            }

            if (!t[lang]) {
                t[lang] = [];
            }

            t[lang][orig] = translated;
        };

        return f;
    },
    lang: (
        window.navigator && (
            (navigator.language || navigator.userLanguage)
                ? (navigator.language || navigator.userLanguage).split("-")[0].toLowerCase()
                : "en"
        )
    ) || "en"
};
